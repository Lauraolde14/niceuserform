package cat.itb.niceuserform_lauraolmedo;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.Calendar;

public class RegisterScreen extends AppCompatActivity {

    Button button_login;
    Button button_register;
    TextInputEditText username;
    TextInputEditText password;
    TextInputEditText repeatPassword;
    TextInputEditText email;
    TextInputEditText name;
    TextInputEditText surnames;
    TextInputEditText birth;
    CheckBox checkBox;
    TextInputLayout dropdown_menu;
    AutoCompleteTextView autoComplete;

    ArrayList<String> arrayList;
    ArrayAdapter<String> arrayAdapter;

    int syear, smonth, day, d, m, y;
    static final  int DATE_ID = 0;
    Calendar c = Calendar.getInstance();

    String u, p, rp, e, n, s, b, a;

    protected void onSaveInstanceState(Bundle save) {
        super.onSaveInstanceState(save);
        u = username.getText().toString();
        p = password.getText().toString();
        rp = repeatPassword.getText().toString();
        e = email.getText().toString();
        n = name.getText().toString();
        s = surnames.getText().toString();
        b = birth.getText().toString();
        a = autoComplete.getText().toString();
        save.putString("user", u);
        save.putString("pass", p);
        save.putString("passwd", rp);
        save.putString("email", e);
        save.putString("name", n);
        save.putString("surnames", s);
        save.putString("birth", b);
        save.putString("gender", a);
    }

    protected void onRestoreInstanceState(Bundle recover){
        super.onRestoreInstanceState(recover);
        u = recover.getString("user");
        p = recover.getString("pass");
        rp = recover.getString("passwd");
        e = recover.getString("email");
        n = recover.getString("name");
        s = recover.getString("surnames");
        b = recover.getString("birth");
        a = recover.getString("gender");
        username.setText(u);
        password.setText(p);
        repeatPassword.setText(rp);
        email.setText(e);
        name.setText(n);
        surnames.setText(s);
        birth.setText(b);
        autoComplete.setText(a);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_screen);

        button_login = findViewById(R.id.button_login);
        button_register = findViewById(R.id.button_register);
        username = findViewById(R.id.usernameText);
        password = findViewById(R.id.passwordText);
        repeatPassword = findViewById(R.id.repeatpasswordText);
        email = findViewById(R.id.emailText);
        name = findViewById(R.id.nameText);
        surnames = findViewById(R.id.surnamesText);
        dropdown_menu = findViewById(R.id.dropdown);
        autoComplete = findViewById(R.id.autoComplete);
        birth = findViewById(R.id.birthdateText);
        checkBox = findViewById(R.id.checkBox);

        smonth = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        syear = c.get(Calendar.YEAR);

        birth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(DATE_ID);
            }
        });

        arrayList = new ArrayList<>();
        arrayList.add("Male");
        arrayList.add("Female");
        arrayList.add("Non binary");

        arrayAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.support_simple_spinner_dropdown_item, arrayList);
        autoComplete.setAdapter(arrayAdapter);
        autoComplete.setThreshold(1);

        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RegisterScreen.this, LoginScreen.class);
                startActivity(i);
            }
        });

        button_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!validateUserame() | !validateName() | !validatePassword() | !validateEmail() | !revalidatePassword() | !validateSurnames() | !validateBirth() | !validateGender() | !validateCheckBox()){
                    return;
                }else {
                    Intent i = new Intent(RegisterScreen.this, WelcomeScreen.class);
                    startActivity(i);
                }
            }
        });
    }

    private  DatePickerDialog.OnDateSetListener DateSetListener =
        new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                y = year;
                m = month;
                d = dayOfMonth;
                birth.setText(d + "-" + (m+1) + "-" + y + " ");
            }
        };

    protected Dialog onCreateDialog(int id){
        switch (id){
            case DATE_ID:
                return new DatePickerDialog(this, DateSetListener, syear, smonth, day);
        }
        return null;
    }

    private boolean validateUserame(){
        String verificarUsername = username.getText().toString();
        if(verificarUsername.isEmpty()){
            username.setError("required field");
            return false;
        }else{
            username.setError(null);
            return true;
        }
    }

    private boolean validatePassword(){
        String verificarPassword = password.getText().toString();
        if(verificarPassword.isEmpty()){
            password.setError("required field");
            return false;
        }else if(verificarPassword.length() < 8){
            password.setError("at least 8 characters");
            return false;
        }else{
            password.setError(null);
            return true;
        }
    }

    private boolean revalidatePassword() {
        String verificarPasswordr = password.getText().toString();
        String verificarPassword = repeatPassword.getText().toString();
        if (verificarPassword.isEmpty()) {
            repeatPassword.setError("required field");
            return false;
        } else if (verificarPassword.length() < 8) {
            repeatPassword.setError("at least 8 characters");
            return false;
        }else if(!verificarPassword.equals(verificarPasswordr)){
            repeatPassword.setError("Different password");
            return false;
        }else{
            repeatPassword.setError(null);
            return true;
        }
    }

    private boolean validateEmail(){
        String verificarEmail = email.getText().toString();
        if(verificarEmail.isEmpty()){
            email.setError("required field");
            return false;
        }else{
            email.setError(null);
            return true;
        }
    }

    private boolean validateName(){
        String verificarName = name.getText().toString();
        if(verificarName.isEmpty()){
            name.setError("required field");
            return false;
        }else{
            name.setError(null);
            return true;
        }
    }

    private boolean validateSurnames(){
        String verificarSurnames = surnames.getText().toString();
        if(verificarSurnames.isEmpty()){
            surnames.setError("required field");
            return false;
        }else{
            surnames.setError(null);
            return true;
        }
    }

    private boolean validateBirth(){
        String verificarBirth = birth.getText().toString();
        if(verificarBirth.isEmpty()){
            birth.setError("required field");
            return false;
        }else{
            birth.setError(null);
            return true;
        }
    }

    private boolean validateGender(){
        String verificarGender = autoComplete.getText().toString();
        if(verificarGender.isEmpty()){
            autoComplete.setError("required field");
            return false;
        }else{
            autoComplete.setError(null);
            return true;
        }
    }

    private  boolean validateCheckBox(){
        if(checkBox.isChecked() == true){
            return true;
        }else{
            checkBox.setError("required field");
            return false;
        }
    }
}