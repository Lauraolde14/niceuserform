package cat.itb.niceuserform_lauraolmedo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class HomeScreen extends AppCompatActivity {

    Button button_login;
    Button button_register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_screen);

        button_login = findViewById(R.id.button_login);
        button_register = findViewById(R.id.button_register);

        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeScreen.this, LoginScreen.class);
                startActivity(i);
            }
        });

        button_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeScreen.this, RegisterScreen.class);
                startActivity(i);
            }
        });
    }

    protected void onSaveInstanceState(Bundle save) {
        super.onSaveInstanceState(save);
    }

    protected void onRestoreInstanceState(Bundle recover){
        super.onRestoreInstanceState(recover);
    }
}