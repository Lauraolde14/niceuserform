package cat.itb.niceuserform_lauraolmedo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class WelcomeScreen extends AppCompatActivity {

    protected void onSaveInstanceState(Bundle save) {
        super.onSaveInstanceState(save);
    }

    protected void onRestoreInstanceState(Bundle recover){
        super.onRestoreInstanceState(recover);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome_screen);
    }
}