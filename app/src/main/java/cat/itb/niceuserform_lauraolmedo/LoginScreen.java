package cat.itb.niceuserform_lauraolmedo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class LoginScreen extends AppCompatActivity {

    Button button_login;
    Button button_register;
    TextInputEditText username;
    TextInputEditText password;
    String u;
    String p;

    protected void onSaveInstanceState(Bundle save) {
        super.onSaveInstanceState(save);
        u = username.getText().toString();
        p = password.getText().toString();
        save.putString("user", u);
        save.putString("pass", p);
    }

    protected void onRestoreInstanceState(Bundle recover){
        super.onRestoreInstanceState(recover);
        u = recover.getString("user");
        p = recover.getString("pass");
        username.setText(u);
        password.setText(p);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen);

        button_login = findViewById(R.id.button_login);
        button_register = findViewById(R.id.button_register);
        username = findViewById(R.id.usernameText);
        password = findViewById(R.id.passwordText);

        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!validateName() | !validatePassword()){
                    return;
                }else {
                    Intent i = new Intent(LoginScreen.this, WelcomeScreen.class);
                    startActivity(i);
                }
            }
        });

        button_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginScreen.this, RegisterScreen.class);
                startActivity(i);
            }
        });
    }

    private boolean validateName(){
        String verificarName = username.getText().toString();
        if(verificarName.isEmpty()){
            username.setError("field can't be empty");
            return false;
        }else{
            username.setError(null);
            return true;
        }
    }

    private boolean validatePassword(){
        String verificarPassword = password.getText().toString();
        if(verificarPassword.isEmpty()){
            password.setError("field can't be empty");
            return false;
        }else if(verificarPassword.length() < 8){
            password.setError("at least 8 characters");
            return false;
        }else{
            password.setError(null);
            return true;
        }
    }
}